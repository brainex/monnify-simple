<?php

namespace Brainex\Monnify;

use Brainex\Monnify\Exceptions\MonnifyRequestException;
use Brainex\Monnify\Exceptions\MonnifySimpleException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\RequestOptions;
use Throwable;

class MonnifySimple
{
    public const AUTH_BASIC      = 'BASIC';
    public const AUTH_TOKEN      = 'TOKEN';

    public const TIMEOUT = 50;

    private string $base_uri = '';

    private string $token = '';

    private string $api_key = '';

    private string $secret_key = '';

    private string $contract_code = '';

    private ?Client $client = null;

    /**
     * Initialize
     *
     * @param string $apikey
     * @param string $secret_key
     * @param string $mode
     */
    public function __construct(string $api_key, string $secret_key, string $contract_code, bool $is_production = false)
    {
        $this->api_key = $api_key;
        $this->secret_key = $secret_key;
        $this->contract_code = $contract_code;

        $this->base_uri = $is_production
            ? 'https://api.monnify.com/api/'
            : 'https://sandbox.monnify.com/api/';

        $this->client = new Client([
            'base_uri' => $this->base_uri,
            'timeout' => self::TIMEOUT,
            RequestOptions::JSON => array(
                'Content-type' => 'application/json'
            )
        ]);
    }

    /**
     * Get banks list
     *
     * @return array
     */
    public function getBanks(): array
    {
        return $this->request('GET', 'v1/banks', self::AUTH_TOKEN)->responseBody;
    }

    /**
     * Reserve account number
     *
     * @param array $payload
     * @see https://developers.monnify.com/api/#create-reserved-accountgeneral
     * @return object
     */
    public function reserveAccount(array $payload)
    {
        $params = array(
            ...$payload,
            'contractCode' => $this->contract_code,
            'currencyCode' => 'NGN',
        );

        return $this->request(
            'POST',
            'v1/bank-transfer/reserved-accounts',
            self::AUTH_TOKEN,
            $params
        )->responseBody;
    }

    /**
     * Initiate bank transfer
     *
     * @param float $amount
     * @param string $reference
     * @param string $narration
     * @param string $bank_code
     * @param string $account_number
     * @param string $source_account_number
     * @return object
     */
    public function transfer(float $amount, string $reference, string $narration, string $bank_code, string $account_number, string $source_account_number)
    {
        return $this->request('POST', 'v2/disbursements/single', self::AUTH_BASIC, [
            'amount' => $amount,
            'reference' => $reference,
            'currency' => 'NGN',
            'narration' => $narration,
            'destinationBankCode' => $bank_code,
            'destinationAccountNumber' => $account_number,
            'sourceAccountNumber' => $source_account_number
        ])->responseBody;
    }

    /**
     * Validate bank account
     *
     * @param string $bank_code
     * @param string $account_number
     * @return object
     */
    public function validateAccount(string $bank_code, string $account_number): object
    {
        return $this->request('GET', 'v1/disbursements/account/validate', self::AUTH_BASIC, [
            'accountNumber' => $account_number,
            'bankCode' => $bank_code
        ])->responseBody;
    }

    /**
     * Verify the bvn on account
     *
     * @param string $account_number
     * @param string $bank_code
     * @param string $bvn
     * @return object
     */
    public function verifyBvnOnAccount(string $account_number, string $bank_code, string $bvn)
    {
        return $this->request('GET', 'v1/verify-bvn-on-account', self::AUTH_TOKEN, [
            'accountNumber' => $account_number,
            'bankCode' => $bank_code,
            'bvn' => $bvn
        ]);
    }

    /**
     * Verify transaction
     *
     * @param string $transaction_reference
     * @return object
     */
    public function verifyTransaction(string $transaction_reference)
    {
        return $this->request('GET', 'v2/transactions/' . $transaction_reference, self::AUTH_TOKEN);
    }

    /**
     * Get Status of Transfer
     *
     * @param string $reference
     * @return object
     */
    public function getTransferStatus(string $reference)
    {
        return $this->request('GET', 'v2/disbursements/single/summary', self::AUTH_TOKEN, [
            'reference' => $reference
        ])->responseBody;
    }

    /**
     * Get wallet balance
     *
     * @param string $account_number
     * @return object
     */
    public function getWalletBalance(string $account_number)
    {
        return $this->request('GET', 'v2/disbursements/wallet-balance', self::AUTH_TOKEN, [
            'accountNumber' => $account_number
        ])->responseBody;
    }

    /**
     * Basic authentication
     *
     * @return object
     */
    protected function authenticate()
    {
        return $this->request('POST', 'v1/auth/login');
    }

    /**
     * Access token
     *
     * @return string
     */
    protected function getAccessToken(): string
    {
        if ($this->token) {
            return $this->token;
        }

        $this->token = $this->authenticate()->responseBody->accessToken;
        return $this->token;
    }

    /**
     * Make http request
     *
     * @param string $method
     * @param string $endpoint
     * @param string $type
     * @param array $payload
     * @return object
     */
    protected function request(string $method, string $endpoint, string $type = self::AUTH_BASIC, array $payload = [])
    {
        $auth = $type === self::AUTH_BASIC
            ? 'Basic ' . base64_encode(implode([$this->api_key, ':', $this->secret_key]))
            : 'Bearer ' . $this->getAccessToken();

        $endpoint .= strtoupper($method) === 'GET'
            ? '?' . http_build_query($payload)
            : '';

        try {

            $response = $this->client->request($method, $endpoint, [
                RequestOptions::JSON => $payload,
                RequestOptions::HEADERS => array(
                    'Authorization' => $auth
                )
            ]);
        } catch (ClientException | ServerException $err) {

            throw new MonnifySimpleException($err->getResponse()->getBody()->getContents());
        } catch (Throwable $e) {
            throw new MonnifyRequestException($e);
        }

        return json_decode($response->getBody()->getContents());
    }
}
